#!/usr/bin/perl
use Device::USB ;

my $timeout = 500 ;
my $buffer = "\0" x 512 ;
my $usb = Device::USB -> new() ;
my $dev = $usb -> find_device ( 0x04a9 , 0x2225 ) ;
$dev -> open() ;
print 'libusb:xxx:' , $dev -> filename() , "\n" ;

@data = control_msg ( $dev , '80 06 00 01 00 00 12 00' ) ;
#printhex ( @data ) ;
my @matrix = ( ) ;
my $line = 0 ;

foreach my $i ( 0..254 )
{
  my $addr = dez2hex ( $i ) ;
  my $data = '01 ' . $addr . ' 01 00' ;
  my $cnt = $dev -> bulk_write ( hex('02') , hex2bin($data) , $timeout ) ;

  if ( $cnt < 0 )
  {
    error ( 'BULK > 0x02 returned error code ' . $cnt ) ;
  }

  my $data = '00 ' x 512 ;
  my $cnt = $dev -> bulk_write ( hex('03') , hex2bin($data) , $timeout ) ;

  if ( $cnt != -2 )
  {
    error ( 'BULK > 0x03 returned error code ' . $cnt ) ;
  }

  my $data = ' ' ;
  my $cnt = $dev -> bulk_read ( hex('83') , $data , 1 , $timeout ) ;

  if ( $cnt < 0 )
  {
    error ( 'BULK < 0x83 returned error code ' . $cnt ) ;
  }

  $data = bin2hex ( $data ) ;
  $data =~ s!00!..! ;
  push ( @matrix , $data ) ;

  if ( @matrix > 15 )
  {
    my $hlin = dez2hex ( 16*$line ) ;
    $hlin =~ s!\s!!g ;
    my $zeile = join ( '' , @matrix ) ;
    $zeile =~ s!\s+\Z!! ;
    print $hlin , ': ' , $zeile , "\n" ;
    @matrix = ( ) ;
    $line ++ ;
  }
}

print 'f0: ' , join ( '' , @matrix ) , "\n" ;
1 ;

sub bin2dez
{
  my ( $data ) = @_ ;
  my @data = unpack ( 'C*' , $data ) ;
  return ( @data )
}

sub dez2bin
{
  my ( @data ) = @_ ;
  my $data = pack ( 'C*' , @data ) ;
  return ( $data )
}

sub hex2dez
{
  my ( $txt ) = @_ ;
  $txt =~ s![\da-f]+:!!ig ;
  $txt =~ s!\A\s+!! ;
  $txt =~ s!\s+\Z!! ;
  my @data = split ( m!\s+! , $txt ) ;

  foreach ( @data )
  {
    $_ = hex ( $_ ) ;
  }

  return ( @data ) ;
}

sub dez2hex
{
  my ( @line ) = @_ ;
  my $anz = @line ;
  my $line = sprintf ( '%02x ' x $anz , @line ) ;
  return ( $line ) ;
}

sub bin2hex
{
  my ( $data ) = @_ ;
  my @data = bin2dez ( $data ) ;
  my $txt = dez2hex ( @data ) ;
  return ( $txt ) ;
}

sub hex2bin
{
  my ( $txt ) = @_ ;
  my @data = hex2dez ( $txt ) ;
  my $data = dez2bin ( @data ) ;
  return ( $data ) ;
}

sub control_msg
{
  my ( $dev , $txt ) = @_ ;
  my ( $typ , $req , $Lv , $Hv , $Li , $Hi , $Ls , $Hs ) = hex2dez ( $txt ) ;
  my $value = $Hv*256 + $Lv ;
  my $index = $Hi*256 + $Li ;
  my $size = $Hs*256 + $Ls ;
  my $buffer = "\0" x $size ;
  my $retval = $dev -> control_msg ( $typ , $req , $value , $index , $buffer , $size , $timeout ) ;
  my @buffer = bin2dez ( $buffer ) ;
  return ( @buffer ) ;
}

sub printhex
{
  my ( @data ) = @_ ;

  while ( @data )
  {
    my @line = splice ( @data , 0 , 16 ) ;
    my $line = dez2hex ( @line ) ;
    print $line , "\n" ;
  }
}

sub display
{
  my ( $dev , $key , $val , $txt ) = @_ ;
  my $pad = ' ' x 22 ;
  $key = substr ( $key . $pad , 0 , 22 ) ;

  if ( $txt == 1 )
  {
    $txt = $dev -> get_string_simple ( $val ) ;
  }

  if ( $txt )
  {
    $txt = ' (' . $txt . ')' ;
  }

  print ( $key , $val , $txt , "\n" ) ;
}

sub error
{
  my ( @msg ) = @_ ;
  $msg[0] .= ' !' ;
  print 'ERROR: ' , join ( "\n" , @msg ) , "\n" ;
  exit ;
}
