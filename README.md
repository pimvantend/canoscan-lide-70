# canoscan-lide-70

An attempt to access the canoscan-lide-70 scanner from linux.
The project started from the files provided at http://www.juergen-ernst.de/info_sane.html .

If you want to make a scan, download from this repository:

1. canonusc8.pl (for scanning)

2. canonusc9.pl (for going back fast)

3. canonusb9.pl (for going back slowly, when stuck)

4. makeppm.pl

5. usbsnoop7009.txt (for scanning)

6. usbsnoop7005.txt (for going back fast)

7. snoop6.txt (for going back slowly, when stuck)

The files *.pl should be made executable.

Command: chmod a+x *.pl

You also need the packages libdevice-usb-perl and libinline-c-perl.

To make a scan, do the following:

1. Plugin the scanner.

2. If the scanner head is not at the home position, use the command: sudo ./canonusc9.pl

   The home position is where you lift the scanner lid.

3. Make a scan: sudo ./canonusc8.pl

4. Problems: retry the command, replug the scanner. If it is not at the home position, send it
there by: sudo ./canonusb9.pl When the slider has problems going back, tilt the scanner to help it.

5. After a succesfull scan run: ./makeppm.pl

6. Display scan.ppm, for example by: gpicview scan.ppm &

7. The scan has a nice resolution of 600 dpi.

8. Bring the slider back to the home position: sudo ./canonusc9.pl
