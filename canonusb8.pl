#!/usr/bin/perl
#use strict ;
use Device::USB;
#use B::Xref

my $debug = 9999;
my $epD;
my $epB;
my $dataD;
my $dataB;
my $infoD;
my $infoB;
my $timeout = 500;
my $buffer  = "\0" x 512;
my $value;
my $scanstarttime = time();

CANON_OpenDevice(0x2225);

#*****************************************************
#          Canon LiDE calibration and scan
#*****************************************************

# at 600 dpi
my $CANON_MAX_WIDTH  = 5104;     # 8.5in		(Letter: 8.5 x 11.0in)
my $CANON_MAX_HEIGHT = 7020;     # 11.7in	(DIN A4: 8.3 x 11.7in)
my $LEFT_EDGE        = 0x01b3;
my $TOP_EDGE         = 0x0041;

my $FLG_GRAY       = 0x01;       # grayscale
my $FLG_FORCE_CAL  = 0x02;       # force calibration
my $FLG_PPM_HEADER = 0x04;       # include PPM header in scan file

my @data = control_msg( $dev, '80 06 00 01 00 00 12 00' );
printhex(@data);

my $line = 0;
DBG( 3, "CANON_start_scan called\n" );
my %scanner = (
    'x1', 0, 'y1', 0, 'x2', 5104, 'y2', 7300, 'resolution', 600, 'fname',
    '/tmp/scan.XXXXXX'
);                               #DIN A4
$scanner{'width'} =
  ( $scanner{'x2'} - $scanner{'x1'} ) * $scanner{'resolution'} / 600;
$scanner{'height'} =
  ( $scanner{'y2'} - $scanner{'y1'} ) * $scanner{'resolution'} / 600;
$scanner{'flags'} = 0;

DBG( 1, "dpi=%d\n",               $scanner{'resolution'} );
DBG( 1, "x1=%d y1=%d\n",          $scanner{'x1'}, $scanner{'y1'} );
DBG( 1, "x2=%d y2=%d\n",          $scanner{'x2'}, $scanner{'y2'} );
DBG( 1, "width=%ld height=%ld\n", $scanner{'width'}, $scanner{'height'} );

print( $scanner{'resolution'} );
print("\n");
print(%scanner);
print("\n");

# choose a temp file name for scan data
#$scanner->{'fname'} = '/tmp/scan.XXXXXX' ;
#print (%scanner) ;
#print ("\n") ;
my $x_start = $LEFT_EDGE;
my $x_end = $LEFT_EDGE + ( $width * 600 / $scanner{'resolution'} ) - 1;
$x_start = $LEFT_EDGE + $scanner{'x1'};
$x_end = $x_start + ( $width * 600 / $scanner{'resolution'} ) - 1;

#  my $y_len = 3 * $height ;
my $y_len = 3 * ( $TOP_EDGE + $scanner{'y2'} );
DBG( 1, "DEBUG 1: %ld %ld - %ld %ld %ld\n",
    $width, $height, $x_start, $x_end, $y_len );
my $y_min = $scanner{'y1'} + $TOP_EDGE;
my $y_max = $scanner{'y2'} + $TOP_EDGE;

my $anyworkdone;
while (1) {
    read_snif( 'usbsnoop7006.txt', 6, 1340 );
    $anyworkdone = store_scan();
    if ( $anyworkdone > 100 ) {
        last;
    }
}
=begin comment
my $hex46 ;
read_snif( 'usbsnoop-back.txt', 1847, 2041 );
while (1) {
            cp2155_get( 0x46, $value );
            $hex46=bin2hex($value);
            unless ($hex46=="37") {
              print $hex46."-";
            }
            if ($hex46=="31 31" || $hex46=="38") {
              last ;
            }
}
=end comment
=cut
my $timeworked = time() - $scanstarttime;
DBG( 4, "scanning took %ld seconds\n", $timeworked );
exit 0;

#------------------------------------------

sub get_urb_data {
    my $state = 'INFO_DOWN';
    my ( $typD, $dirD, $epD, $dataD, $infoD ) = ();
    my ( $typB, $dirB, $epB, $dataB, $infoB ) = ();

    while ( $zeile = <INDAT> ) {
        chomp($zeile);

        if ( substr( $zeile, 0, 1 ) eq '#' ) {
            next();
        }

        if ( $state eq 'INFO_DOWN' ) {
            unless ($zeile) {
                next();
            }

            ( $typD, $dirD, $epD ) = split( m! +!, $zeile );
            $infoD = $zeile;

            unless ( $typD eq 'BULK' ) {
                error( 'Type does not match BULK', $zeile );
            }

            $state = 'DATA_DOWN';
            $dataD = '';
        }
        elsif ( $state eq 'DATA_DOWN' ) {
            if ( substr( $zeile, 12, 1 ) eq ':' ) {
                $dataD .= $zeile;
            }
            elsif ($zeile) {
                ( $typB, $dirB, $epB ) = split( m! +!, $zeile );
                $infoB = $zeile;

                unless ( $typB eq 'BULK' ) {
                    error( 'Type does not match BULK', $infoD, $zeile );
                }

                $state = 'DATA_BACK';
                $dataB = '';
            }
            else {
                last();
            }
        }
        elsif ( $state eq 'DATA_BACK' ) {
            if ( substr( $zeile, 12, 1 ) eq ':' ) {
                $dataB .= $zeile;
            }
            elsif ($zeile) {
                error( 'Empty line expected', $infoD, $infoB, $zeile );
            }
            else {
                last();
            }
        }
        else {
            error( 'Illegal state', $infoD, $zeile );
        }
    }

    $dataD = dez2hex( hex2dez($dataD) );
    $dataB = dez2hex( hex2dez($dataB) );
    return ( $epD, $dataD, $infoD, $epB, $dataB, $infoB );
}

sub CANON_OpenDevice {
    my ($id) = @_;
    $usb = Device::USB->new();
    $dev = $usb->find_device( 0x04a9, $id );
    $cfg = $dev->get_configuration(0);

    $cfgval = $cfg->bConfigurationValue();
    $err    = $dev->set_configuration($cfgval);
    print $err, "\n";

    $dev->open();
    $dev->claim_interface();
    print 'libusb:xxx:', $dev->filename(), "\n";
}

sub DBG {
    my ( $level, $format, @msg ) = @_;

    if ( $level <= $debug ) {
        printf( $format , @msg );
    }
}

#------------------------------------------

sub bin2dez {
    my ($data) = @_;
    my @data = unpack( 'C*', $data );
    return (@data);
}

sub dez2bin {
    my (@data) = @_;
    my $data = pack( 'C*', @data );
    return ($data);
}

sub hex2dez {
    my ($txt) = @_;
    $txt =~ s![\da-f]+:!!ig;
    $txt =~ s!\A\s+!!;
    $txt =~ s!\s+\Z!!;
    my @data = split( m!\s+!, $txt );

    foreach (@data) {
        $_ = hex($_);
    }

    return (@data);
}

sub dez2hex {
    my (@line) = @_;
    my $anz = @line;
    my $line = sprintf( '%02x ' x $anz, @line );
    return ($line);
}

sub bin2hex {
    my ($data) = @_;
    my @data   = bin2dez($data);
    my $txt    = dez2hex(@data);
    return ($txt);
}

sub hex2bin {
    my ($txt) = @_;
    my @data  = hex2dez($txt);
    my $data  = dez2bin(@data);
    return ($data);
}

sub control_msg {
    my ( $dev, $txt ) = @_;
    my ( $typ, $req, $Lv, $Hv, $Li, $Hi, $Ls, $Hs ) = hex2dez($txt);
    my $value  = $Hv * 256 + $Lv;
    my $index  = $Hi * 256 + $Li;
    my $size   = $Hs * 256 + $Ls;
    my $buffer = "\0" x $size;
    my $retval =
      $dev->control_msg( $typ, $req, $value, $index, $buffer, $size, $timeout );
    my @buffer = bin2dez($buffer);
    return (@buffer);
}

sub printhex {
    my (@data) = @_;

    while (@data) {
        my @line = splice( @data, 0, 16 );
        my $line = dez2hex(@line);
        print $line , "\n";
    }
}

sub newopen {
    my ($fname) = @_;
    local *FILEHANDLE;

    unless ( open( FILEHANDLE, $fname ) ) {
        return (undef);
    }

    return (*FILEHANDLE);
}

sub usleep {
    my ($time) = @_;
    select( undef, undef, undef, $time );
}

sub store_scan {
    my $fp = newopen( '>' . $scanner{'fname'} );
    DBG( 4, "writing %s\n", $scanner{'fname'} );
    printf $fp ( "P6\n%ld %ld\n255\n", $scanner{'width'}, $scanner{'height'} );
    my $width  = $scanner{'width'};
    my $height = $scanner{'height'};
    DBG( 1, "width height %ld %ld \n", $width, $height );

    # set width to next multiple of 0x10
    while ( $width & 0x0f ) {
        $width++;
    }
    my $timeswaited = 0;
    while (1) {
        my $datasize = wait_for_data();
        $timeswaited += 1;
        DBG( 12, "datasize %ld timeswaited %ld \n", $datasize, $timeswaited );

        if ( $datasize < 0 ) {
            DBG( 1, "no data\n" );
            cp2155_get( 0x46, $value );
            print bin2hex($value) . "\n";
            last();
        }

        if ( $datasize > 32000 ) {
            $datasize = 32000;
        }

        #        DBG( 12, "scanning %ld %ld %ld\n", $line, $height, $datasize );

        #    DBG ( 12 , "width %ld\n" , $width ) ;
        cp2155_get( 0x46, $value );
        print '0x46 = ' . bin2hex($value) . "\n";

        cp2155_set( 0x72, ( $datasize >> 8 ) & 0xff );
        cp2155_set( 0x73, ($datasize) & 0xff );

        my $status = cp2155_read( $datasize, $readbuf );

        if ( $status ne 'SANE_STATUS_GOOD' ) {
            $status = 'SANE_STATUS_INVAL';
            last();
        }
        print $fp $readbuf;
    }
    close($fp);
    return ($timeswaited);
}

sub read_snif {
    my ( $file, $urb_start, $urb_ende ) = @_;
    open( INDAT, '<' . $file );
    while ( !eof(INDAT) ) {
        ( $epD, $dataD, $infoD, $epB, $dataB, $infoB ) = get_urb_data();
        my @urb = split( m![\(\)\s]+!, $infoD );

        if ( $urb[3] eq 'URB' ) {
            if ( $urb_start && ( $urb[4] < $urb_start ) ) {
                next();
            }

            if ( $urb_ende && ( $urb[4] > $urb_ende ) ) {
                last();
            }
        }

        print $infoD , "\n";
        my $cnt = $dev->bulk_write( hex($epD), hex2bin($dataD), $timeout );

        if ( ( $cnt < 0 ) && ( $epD != '03' ) ) {
            error( 'BULK > 0x' . $epD . ' returned error code ' . $cnt );
            print 'error: ', $cnt, "\n";
        }

        if ($epB) {
            print $infoB ;
            my $data = ' ';
            my $cnt = $dev->bulk_read( hex($epB), $data, 1, $timeout );

            if ( $cnt < 0 ) {
                print "\n";
                error( 'BULK < 0x' . $epB . ' returned error code ' . $cnt );
            }

            $data = bin2hex($data);
            print ' ret=', $data, "\n";
        }
    }
    close(INDAT);
}

#*****************************************************
#            CP2155 communication primitives
#   Provides I/O routines to Philips CP2155BE chip
#*****************************************************

# Write single byte to CP2155 register

sub cp2155_set {
    my ( $reg, $val ) = @_;
    my @data = ( ( $reg >> 8 ) & 0xff, ( $reg & 0xff ), 0x01, 0x00, $val );
    my $status = $dev->bulk_write( 0x02, dez2bin(@data), $timeout );

    if ( $status < 0 ) {
        DBG( 1, "cp2155_set: sanei_usb_write_bulk error\n" );
        return ('SANE_STATUS_IO_ERROR');
    }

    return ('SANE_STATUS_GOOD');
}

# Read single byte from CP2155 register
sub cp2155_get {
    my $reg    = $_[0];
    my @data   = ( 0x01, $reg, 0x01, 0x00 );
    my $status = $dev->bulk_write( 0x02, dez2bin(@data), $timeout );

    if ( $status < 0 ) {
        DBG( 1, "cp2155_get: sanei_usb_write_bulk error\n" );
        return ('SANE_STATUS_IO_ERROR');
    }

    usleep(0.001);

    my $data = ' ';
    my $status = $dev->bulk_read( 0x83, $data, 1, $timeout );
    $_[1] = ord($data);

    if ( $status < 0 ) {
        DBG( 1, "cp2155_get: sanei_usb_read_bulk error\n" );
        return ('SANE_STATUS_IO_ERROR');
    }

    return ('SANE_STATUS_GOOD');
}

# Write a block of data to CP2155 chip
sub cp2155_write {
    my ( $count, @data ) = @_;
    my @head = ( 0x04, 0x70, ( $count & 0xff ), ( $count >> 8 ) & 0xff );

    while ( @data < $count ) {
        push( @data, 0x00 );
    }

    my $data = dez2bin( @head, @data );
    my $status = $dev->bulk_write( 0x02, $data, $timeout );

    if ( $status < 0 ) {
        DBG( 1, "cp2155_write: sanei_usb_write_bulk error\n" );
        return ('SANE_STATUS_IO_ERROR');
    }

    return ('SANE_STATUS_GOOD');
}

# Read a block of data from CP2155 chip
sub cp2155_read {

    #  my ( $size , $data ) = @_ ;
    my $size   = $_[0];
    my @data   = ( 0x05, 0x70, ( $size & 0xff ), ( $size >> 8 ) & 0xff );
    my $status = $dev->bulk_write( 0x02, dez2bin(@data), $timeout );

    if ( $status < 0 ) {
        DBG( 1, "cp2155_get: sanei_usb_write_bulk error\n" );
        return ('SANE_STATUS_IO_ERROR');
    }

    #  usleep ( 0.001 ) ;

    $_[1] = "\0" x $size;
    my $status = $dev->bulk_read( 0x83, $_[1], $size, $timeout );

    if ( $status < 0 ) {
        DBG( 1, "cp2155_read: sanei_usb_read_bulk error %d\n", $size );
        return ('SANE_STATUS_IO_ERROR');
    }

    return ('SANE_STATUS_GOOD');
}

sub error {
    my (@msg) = @_;
    $msg[0] .= ' !';
    print 'ERROR: ', join( "\n", @msg ), "\n";
    exit;
}

# Wait until data ready
sub wait_for_data {
    my $start_time = time();
    my $value      = '';

    #    DBG( 12, "waiting...\n" );

    while (1) {
        my $size = 0;

        if ( cp2155_get( 0xa5, $value ) ne 'SANE_STATUS_GOOD' ) {
            return (-1);
        }

        $size += $value;

        if ( cp2155_get( 0xa6, $value ) ne 'SANE_STATUS_GOOD' ) {
            return (-1);
        }

        $size <<= 8;
        $size += $value;

        if ( cp2155_get( 0xa7, $value ) ne 'SANE_STATUS_GOOD' ) {
            return (-1);
        }

        $size <<= 8;
        $size += $value;

        if ( $size != 0 ) {
            return ( 2 * $size );
        }

        # Give it 5 seconds, changed to 0.05
        if ( ( time() - $start_time ) > 0.05 ) {
            DBG( 1, "wait_for_data: timed out (%ld)\n", $size );
            return (-1);
        }

        usleep(0.004);
    }
}

#------------------------------------------

#check for pressed buttons
sub read_buttons {
    cp2155_get( 0x91, $value );
    DBG( 1, "0x91: %02x\n", $value );
}

