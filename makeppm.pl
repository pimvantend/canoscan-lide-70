#!/usr/bin/perl
my $starttimemake = time () ;
my $fp1 = newopen ( '<' . '/tmp/scan.XXXXXX' ) ;
  unless ( $fp1 )
  {
    my $errno = $! ;
#    DBG ( 1 , "err:%s when opening %s\n" , $errno , $scanner{'fname'} ) ;
#    return ( 'SANE_STATUS_IO_ERROR' ) ;
  }

#DBG ( 4 , "reading %s\n" , $scanner{'fname'} ) ;

# Convert data file from line-by-line RGB to byte-by-byte RGB,
open ( OUTDAT , '>scan.ppm' ) ;
my $buf = '' ;
my $anzlf = 0 ;

for ( my $idx = 0 ; $idx < 32 ; $idx ++ )
{
  my $char = '' ; read ( $fp1 , $char , 1 ) ; 
#print $char ;
  $buf .= $char ;

  if ( $char eq "\n" )
  {
    $anzlf ++ ;

    if ( $anzlf >= 3 )
    {
      last ( ) ;
    }
  }
}
#print $buf ;
my ( $format , $size , $colors ) = split ( m!\n! , $buf ) ;
my ( $xmax , $ymax ) = split ( m!\s+! , $size ) ;
$ymax-=1 ;
$size=$xmax.' '.$ymax ;
print OUTDAT $format , "\n" ;
print OUTDAT $size   , "\n" ;
print OUTDAT $colors , "\n" ;
print  $format , "\n" ;
print  $size   , "\n" ;
print  $colors , "\n" ;

for ( my $y = 0 ; $y < $ymax ; $y ++ )
{
  my $rbuf = '' ; read ( $fp1 , $rbuf , $xmax ) ;
  my $gbuf = '' ; read ( $fp1 , $gbuf , $xmax ) ;
  my $bbuf = '' ; read ( $fp1 , $bbuf , $xmax ) ;

  for ( my $x = 0 ; $x < $xmax ; $x ++ )
  {
    print OUTDAT ( substr ( $rbuf , $x , 1 ) , substr ( $gbuf , $x , 1 ) , substr ( $bbuf , $x , 1 ) ) ;
  }
}

close ( OUTDAT ) ;
printf "\nmakeppm.pl took %ld seconds\n", time () - $starttimemake ;

sub newopen
{
  my ( $fname ) = @_ ;
  local *FILEHANDLE ;
  
  unless ( open ( FILEHANDLE , $fname ) )
  {
    return ( undef ) ;
  }

  return ( *FILEHANDLE ) ;
}

